name := """neophytes"""

version := "1.0"

scalaVersion := "2.11.5"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Change this to another test framework if you prefer
libraryDependencies += "org.specs2" %% "specs2-core" % "3.0.1" % "test"

scalacOptions in Test ++= Seq("-Yrangepos")

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.3.9"

