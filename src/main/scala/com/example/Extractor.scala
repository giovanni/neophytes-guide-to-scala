package com.example

object Extractor extends App {
  case class UserCase(firstName: String, lastName: String, score: Int)
  def advance(xs: List[UserCase]) = xs match {
    case UserCase(_, _, score1) :: UserCase(_, _, score2) :: _ => score1 - score2
    case _ => 0
  }

  val result = advance(List(UserCase("John","Ive", 100), UserCase("Steve", "Jobs", 110)))
  println(result)

  def greetUser(user: User) = user match {
    case FreeUser(name, _, _) => s"Hello, $name"
    case PremiumUser(name, _) => s"Welcome back dear $name"
  }

  def greetUserScore(user: User) = user match {
    case FreeUser(name, _, p) =>
      if (p > 0.75) name + ", what can we do for you today?" else "Hello " + name
    case PremiumUser(name, _) => "Welcome back, dear " + name
  }

  def sendEmail(user: User) = user match {
    case freeUser @ premiumCandidate() => initiateSpamProgram(freeUser)
    case _ => sendRegularNewsletter(user)
  }

  def initiateSpamProgram(user: FreeUser) = "This is a spam"
  def sendRegularNewsletter(user: User) = "Regular news letter"

  val users: List[User] = new FreeUser("Alan", 3000, 0.7d) :: new PremiumUser("Delan", 10) :: Nil

  users.map(greetUser).foreach(println)
}

trait User {
  def name: String
  def score: Int
}
class FreeUser(val name: String, val score: Int, val upgradeProbability: Double) extends User
class PremiumUser(val name: String, val score: Int) extends User

object FreeUser {
  def unapply(user: FreeUser): Option[(String, Int, Double)] = Some((user.name, user.score, user.upgradeProbability))
}
object PremiumUser {
  def unapply(user: PremiumUser): Option[(String, Int)] = Some((user.name, user.score))
}

object premiumCandidate {
  def unapply(user: FreeUser): Boolean = user.upgradeProbability > 0.75
}