import org.specs2._
import com.example._
class ExtractorSpec extends mutable.Specification {

  "Extractor Specification http://danielwestheide.com/blog/2012/11/21/the-neophytes-guide-to-scala-part-1-extractors.html\n".txt

  val freeUser = new FreeUser("Alan", 3000, 0.78d)
  val premiumUser = new PremiumUser("Delonge", 1000)
  val freeUser2 = new FreeUser("Alan No Score", 2000, 0.4d)

  "The Extractor should" >> {
    "Diferentiate FreeUser from Premium user in greetUser function" >> {
      Extractor.greetUser(freeUser) must equalTo("Hello, Alan")
      Extractor.greetUser(premiumUser) must equalTo("Welcome back dear Delonge")
    }
    "Extract user with logic" >> {
      Extractor.greetUserScore(freeUser) must equalTo("Alan, what can we do for you today?")
      Extractor.greetUserScore(premiumUser) must equalTo("Welcome back, dear Delonge")
      Extractor.greetUserScore(freeUser2) must equalTo("Hello Alan No Score")
    }

    "Use a Boolean extractor" >> {
      Extractor.sendEmail(freeUser) must equalTo("This is a spam")
      Extractor.sendEmail(freeUser2) must equalTo("Regular news letter")
      Extractor.sendEmail(premiumUser) must equalTo("Regular news letter")
    }
  }
}
